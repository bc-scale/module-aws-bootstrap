variable "vendor_prefix" {
  description = "Initials of the company which the project is bootstrapped."
  type        = string
  default     = "vendor"
}

variable "s3_bucket_prefix" {
  description = "Prefix of the bucket that will contain terraform state files."
  type        = string
  default     = "vendor-tfstate"
}

variable "tags" {
  description = "Additional tags to add to all AWS resources. Will be merged with the default tags provided by this module."
  type        = map(string)
  default     = {}
}

#####
# IAM Policies & Roles
#####

variable "subdirectories_by_teams" {
  description = "Object containing a team name as key and a sub-directory as value. For each team, a specific policy will be created to allow access only to the given sub-directory."
  type        = map(string)
  default     = {}
}

variable "attach_subdirectories_policy_to_existing_groups" {
  description = "If true, the module will search for existing groups by the names passed in var.subdirectories_by_teams and attach the specific policy to the specific group."
  type        = bool
  default     = false
}

variable "allowed_aws_user_id_patterns" {
  description = <<-EOT
List of AWS user ID patterns that will be allowed to access the bucket.
All ID's that do not match that pattern will be denied access to the bucket.
If `attach_subdirectories_policy_to_existing_groups` is true, these will automatically be allowed.
[More Information](https://aws.amazon.com/blogs/security/how-to-restrict-amazon-s3-bucket-access-to-a-specific-iam-role/)
EOT
  type        = list(string)
  default     = []
}

variable "trusted_aws_accounts" {
  description = "List of additional AWS account which will be trusted to assume the s3 role."
  type        = list(string)
  default     = []
}

variable "trusted_aws_iam_identifiers" {
  description = "List of additional AWS IAM identifiers which will be trusted to assume the s3 role. (ex: A specific role in an other account)"
  type        = list(string)
  default     = []
}
