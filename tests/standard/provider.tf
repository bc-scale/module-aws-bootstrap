provider "aws" {
  access_key = local.aws_access_key
  region     = "us-east-1"
  secret_key = local.aws_secret_key
  profile    = local.aws_profile

  s3_force_path_style         = local.localstack_test
  skip_credentials_validation = local.localstack_test
  skip_metadata_api_check     = local.localstack_test
  skip_requesting_account_id  = local.localstack_test

  assume_role {
    role_arn = local.aws_assume_role
  }

  endpoints {
    apigateway     = local.endpoint
    cloudformation = local.endpoint
    cloudwatch     = local.endpoint
    dynamodb       = local.endpoint
    ec2            = local.endpoint
    es             = local.endpoint
    firehose       = local.endpoint
    iam            = local.endpoint
    kinesis        = local.endpoint
    kms            = local.endpoint
    lambda         = local.endpoint
    route53        = local.endpoint
    redshift       = local.endpoint
    s3             = local.endpoint
    secretsmanager = local.endpoint
    ses            = local.endpoint
    sns            = local.endpoint
    sqs            = local.endpoint
    ssm            = local.endpoint
    stepfunctions  = local.endpoint
    sts            = local.endpoint
  }
}
