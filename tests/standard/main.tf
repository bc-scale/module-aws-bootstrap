resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

module "s3_bootstrap" {
  source = "../../"

  vendor_prefix    = random_string.this.result
  s3_bucket_prefix = random_string.this.result
}
