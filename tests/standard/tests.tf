resource "test_assertions" "s3_bucket" {
  component = "standard-s3_bucket"

  check "bucket_name" {
    description = "bucket name should match the passed prefix"
    condition   = can(regex("^${random_string.this.result}", module.s3_bootstrap.s3_id))
  }

  check "bucket_arn" {
    description = "bucket arn should be an AWS arn"
    condition   = can(regex("^arn:aws", module.s3_bootstrap.s3_arn))
  }

  check "bucket_bucket_domain_name" {
    description = "bucket domain name should be a domain name"
    condition   = can(regex(random_string.this.result, module.s3_bootstrap.s3_bucket_domain_name))
  }

  equal "bucket_region" {
    description = "bucket region should be us-east-1"
    got         = module.s3_bootstrap.s3_region
    want        = "us-east-1"
  }
}

resource "test_assertions" "kms_key" {
  component = "standard-kms_key"

  check "kms_arn" {
    description = "key arn should be an AWS arn"
    condition   = can(regex("^arn:aws", module.s3_bootstrap.s3_arn))
  }
}

resource "test_assertions" "dynamodb_table" {
  component = "standard-dynamodb_table"

  check "dynamodb_arn" {
    description = "DynamoDB arn should be an AWS arn"
    condition   = can(regex("^arn:aws", module.s3_bootstrap.dynamodb_arn))
  }

  check "dynamodb_name" {
    description = "DynamoDB name should match prefix"
    condition   = can(regex("^${random_string.this.result}", module.s3_bootstrap.dynamodb_name))
  }
}

resource "test_assertions" "iam" {
  component = "standard-iam"

  check "iam_policy_ids_count" {
    description = "should output more then 1 policy"
    condition   = length(module.s3_bootstrap.iam_policy_ids) == 1
  }

  check "iam_policy_id_all" {
    description = "there should be an all key"
    condition   = can(lookup(module.s3_bootstrap.iam_policy_ids, "all"))
  }

  check "iam_policy_arns_count" {
    description = "should output more then 1 policy arn"
    condition   = length(module.s3_bootstrap.iam_policy_arns) == 1
  }

  check "iam_policy_arn_all" {
    description = "there should be an all key"
    condition   = can(lookup(module.s3_bootstrap.iam_policy_arns, "all"))
  }

  check "iam_role_ids_count" {
    description = "should output more then 1 role"
    condition   = length(module.s3_bootstrap.iam_role_ids) == 1
  }

  check "iam_role_id_all" {
    description = "there should be an all key"
    condition   = can(lookup(module.s3_bootstrap.iam_role_ids, "all"))
  }

  check "iam_role_arns_count" {
    description = "should output more then 1 role arn"
    condition   = length(module.s3_bootstrap.iam_role_arns) == 1
  }

  check "iam_role_arn_all" {
    description = "there should be an all key"
    condition   = can(lookup(module.s3_bootstrap.iam_role_arns, "all"))
  }
}
