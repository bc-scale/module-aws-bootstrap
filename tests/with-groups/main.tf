resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

resource "aws_iam_group" "test" {
  name = "test"
}

resource "aws_iam_user" "test" {
  name = "${random_string.this.result}test"
}

resource "aws_iam_group_membership" "member" {
  group = aws_iam_group.test.id
  name  = aws_iam_user.test.id
  users = [aws_iam_user.test.id]
}
module "s3_bootstrap" {
  source = "../../"

  vendor_prefix                                   = random_string.this.result
  s3_bucket_prefix                                = random_string.this.result
  attach_subdirectories_policy_to_existing_groups = true
  subdirectories_by_teams = {
    test : "test",
  }
  trusted_aws_accounts = [
    "1234567890"
  ]

  depends_on = [aws_iam_group_membership.member]
}
