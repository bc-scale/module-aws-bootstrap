locals {
  aws_access_key  = "fake"
  aws_secret_key  = "fake"
  endpoint        = "http://localstack:4566"
  localstack_test = true
  aws_profile     = null
  aws_assume_role = null
}
