# CHANGELOG

## 5.4.0

* feat: Add dynamoDB table for statelocking

## 5.3.1

* fix: the "all" role didn't provide access on the objects of the s3 bucket

## 5.3.0

* feat: Add variables to allow external AWS principals on s3 role

## 5.2.0

* feat: ensure all public access policies are ignored
* fix: set explicit KMS policy permissions
* chore: Improve pipeline and testing
* chore: Add tfsec tests and results
* chore: Add additional test for better coverage
* chore: Update LICENSE file

## 5.1.1

* fix: Add condition on adding groups in access-policy

## 5.1.0

* chore: update pre-commit dependencies
* chore: allows terraform 1+

## 5.0.0

* feat: (BREAKING) remove terraform protection policy, as it is now in another module

## 4.2.2

* fix: allows anything in an account to assume the S3 roles, as the S3 assume role is useless

## 4.2.1

* fix: makes sure a caller ID that is not a role can still access the bucket
* fix: makes sure group users can access the bucket
* test: also test `vat.attach_subdirectories_policy_to_existing_groups` to `true`

## 4.2.0

* feat: Add role per team (subdirectory) for fine grained access
* feat: Add better tag handling
* chore: Update pre-commit configuration
* feat: Enable key rotation by default on KMS
* fix: Add current caller and teams role towhitelist to bucket
* chore: Add env variable to CI

## 4.1.0

* feat: Add bucket policy to explicitly deny access
* chore: Add mega-linter to gitlab-ci
* chore: Add localstack service in CI
* chore: add gitlab-ci file for making tests

## 4.0.0

* feat: adds a policy that prevent any specific action on a "managed-by=terraform" resource
* chore: updates pre-commit pinned version
* refactor(BREAKING): ouputs all policies information in objects

## 3.0.0

* feat: adds support for Terraform 0.15
* test: revamps the example

## 2.1.1

* fix: fixes wrong IAM policy for read/write access with KMS key

## 2.1.0

* feat: allows to create X policies to restrict accesses to sub-directories in the S3 bucket
* refactor: moves data to `data.tf`
* fix: makes sure the policy created allows to access the KMS key use to encrypt/decrypt the S3 bucket content

## 2.0.0

* chore: updates pre-commit dependencies
* maintenance: updates minimal terraform version to 0.14, minimal AWS provider to 3.35
* feat: (BREAKING) removes s3_bucket variable as it uses the provider region now.

## 1.1.0

* feat: Add possibility to add custom tags on resources
