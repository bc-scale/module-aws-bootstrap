output "s3_id" {
  description = "The ID of the S3 bucket for terraform state files."
  value       = aws_s3_bucket.terraform_bucket.id
}

output "s3_arn" {
  description = "The ARN of the S3 bucket for terraform state files."
  value       = aws_s3_bucket.terraform_bucket.arn
}

output "s3_bucket_domain_name" {
  description = "The domain name of S3 bucket for terraform state files."
  value       = aws_s3_bucket.terraform_bucket.bucket_domain_name
}

output "s3_region" {
  description = "The region of S3 bucket for terraform state files."
  value       = aws_s3_bucket.terraform_bucket.region
}

output "kms_id" {
  description = "The key id for the S3 bucket for terraform state files."
  value       = aws_kms_key.terraform_bucket.key_id
}

output "kms_arn" {
  description = "The key ARN for the S3 bucket for terraform state files."
  value       = aws_kms_key.terraform_bucket.arn
}

output "dynamodb_arn" {
  description = "The ARN of the dynamoDB for terraform state locks."
  value       = aws_dynamodb_table.this.arn
}

output "dynamodb_id" {
  description = "The ID of the dynamoDB for terraform state locks."
  value       = aws_dynamodb_table.this.id
}

output "dynamodb_name" {
  description = "The name of the dynamoDB for terraform state locks."
  value       = aws_dynamodb_table.this.name
}

#####
# IAM Policies & Roles
#####

output "iam_policy_ids" {
  value = merge(
    {
      all : aws_iam_policy.terraform_bucket.id,
    },
    {
      for key, policy in aws_iam_policy.terraform_subdirectory : key => policy.id
    }
  )
}

output "iam_policy_arns" {
  value = merge(
    {
      all : aws_iam_policy.terraform_bucket.arn,
    },
    {
      for key, policy in aws_iam_policy.terraform_subdirectory : key => policy.arn
    }
  )
}

output "iam_role_ids" {
  value = merge(
    {
      all : aws_iam_role.terraform_bucket.id
    },
    {
      for key, role in aws_iam_role.terraform_bucket_teams : key => role.id
    }
  )
}

output "iam_role_arns" {
  value = merge(
    {
      all : aws_iam_role.terraform_bucket.arn
    },
    {
      for key, role in aws_iam_role.terraform_bucket_teams : key => role.arn
    }
  )
}

output "iam_role_unique_ids" {
  value = merge(
    {
      all : aws_iam_role.terraform_bucket.unique_id
    },
    {
      for key, role in aws_iam_role.terraform_bucket_teams : key => role.unique_id
    }
  )
}
