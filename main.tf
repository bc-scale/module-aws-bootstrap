locals {
  tags = {
    managed-by  = "terraform"
    environment = "root"
    origin      = "gitlab.com/wild-beavers/terraform/module-aws-bootstrap"
  }
}

resource "aws_kms_key" "terraform_bucket" {
  description             = "This key is used to encrypt terraform state file bucket."
  deletion_window_in_days = 10
  enable_key_rotation     = true

  tags = merge(
    {
      Name = "KMS key for S3 bucket for terraform state files."
    },
    local.tags,
    var.tags
  )
}

resource "aws_kms_key" "terraform_dynamodb" {
  description             = "This key is used to encrypt terraform state locks in dynamodb."
  deletion_window_in_days = 7
  enable_key_rotation     = true

  tags = merge(
    {
      Name = "KMS key for dynamodb terraform state locks."
    },
    local.tags,
    var.tags
  )
}

resource "aws_s3_bucket" "terraform_bucket" {
  bucket_prefix = var.s3_bucket_prefix
  acl           = "private"

  tags = merge(
    {
      Name = "Terraform state files bucket."
    },
    local.tags,
    var.tags
  )

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.terraform_bucket.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "terraform_bucket" {
  bucket = aws_s3_bucket.terraform_bucket.id
  policy = data.aws_iam_policy_document.bucket_policy.json
}

resource "aws_s3_bucket_public_access_block" "terraform_bucket" {
  bucket                  = aws_s3_bucket.terraform_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

#####
# IAM  Roles
#####

resource "aws_iam_role" "terraform_bucket" {
  name        = "${upper(var.vendor_prefix)}S3TerraformRole"
  description = "Role for Terraform state files bucket."

  tags = merge(
    {
      Name = "Role for Terraform state files bucket."
    },
    local.tags,
    var.tags
  )

  assume_role_policy = data.aws_iam_policy_document.account_assume_role.json
}

resource "aws_iam_role_policy_attachment" "terraform_bucket" {
  role       = aws_iam_role.terraform_bucket.name
  policy_arn = aws_iam_policy.terraform_bucket.arn
}

resource "aws_iam_role" "terraform_bucket_teams" {
  for_each = var.subdirectories_by_teams

  name        = "${upper(var.vendor_prefix)}${upper(each.key)}S3TerraformRole"
  description = "Role for Terraform state files bucket for ${each.key}."

  tags = merge(
    {
      Name = "${each.key} team role for Terraform state files bucket."
    },
    local.tags,
    var.tags
  )

  assume_role_policy = data.aws_iam_policy_document.account_assume_role.json
}

resource "aws_iam_role_policy_attachment" "terraform_bucket_teams" {
  for_each = var.subdirectories_by_teams

  role       = aws_iam_role.terraform_bucket_teams[each.key].name
  policy_arn = aws_iam_policy.terraform_subdirectory[each.key].arn
}

#####
# IAM Policies
#####

resource "aws_iam_policy" "terraform_bucket" {
  name   = "${upper(var.vendor_prefix)}S3TerraformFullAccess"
  path   = "/"
  policy = data.aws_iam_policy_document.allow_all.json
  tags   = merge(local.tags, var.tags)
}

resource "aws_iam_policy" "terraform_subdirectory" {
  for_each = var.subdirectories_by_teams

  name   = format("%sS3Terraform%sAccess", upper(var.vendor_prefix), each.key)
  path   = "/"
  policy = data.aws_iam_policy_document.limited_access[each.key].json
  tags   = merge(local.tags, var.tags)
}

#####
# IAM Group
#####

resource "aws_iam_group_policy_attachment" "groups" {
  count = var.attach_subdirectories_policy_to_existing_groups ? length(keys(var.subdirectories_by_teams)) : 0

  group      = keys(var.subdirectories_by_teams)[count.index]
  policy_arn = aws_iam_policy.terraform_subdirectory[keys(var.subdirectories_by_teams)[count.index]].arn
}

#####
# DynamoDB Table
#####

resource "aws_dynamodb_table" "this" {
  name           = format("%s-tf-lock", var.s3_bucket_prefix)
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"

  # Because these are temporary locks, there is no need to enable
  # PIT recovery on the table.
  point_in_time_recovery {
    enabled = false #tfsec:ignore:aws-dynamodb-enable-recovery
  }

  server_side_encryption {
    enabled     = true
    kms_key_arn = aws_kms_key.terraform_dynamodb.arn
  }

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = merge(
    {
      Name = "DynamoDB Terraform State Lock Table"
    },
    local.tags,
    var.tags
  )
}
