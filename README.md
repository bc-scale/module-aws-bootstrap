# Terraform module: bootstrap S3

Bootstrap terraform AWS S3 repository.

This module should be used in a client-specific bootstrap terraform module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.35 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.35 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_group_policy_attachment.groups](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_policy.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.terraform_subdirectory](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.terraform_bucket_teams](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.terraform_bucket_teams](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_kms_key.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.terraform_dynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_s3_bucket.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_caller_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_group.groups](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_group) | data source |
| [aws_iam_policy_document.account_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.allow_all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.bucket_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.limited_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_aws_user_id_patterns"></a> [allowed\_aws\_user\_id\_patterns](#input\_allowed\_aws\_user\_id\_patterns) | List of AWS user ID patterns that will be allowed to access the bucket.<br>All ID's that do not match that pattern will be denied access to the bucket.<br>If `attach_subdirectories_policy_to_existing_groups` is true, these will automatically be allowed.<br>[More Information](https://aws.amazon.com/blogs/security/how-to-restrict-amazon-s3-bucket-access-to-a-specific-iam-role/) | `list(string)` | `[]` | no |
| <a name="input_attach_subdirectories_policy_to_existing_groups"></a> [attach\_subdirectories\_policy\_to\_existing\_groups](#input\_attach\_subdirectories\_policy\_to\_existing\_groups) | If true, the module will search for existing groups by the names passed in var.subdirectories\_by\_teams and attach the specific policy to the specific group. | `bool` | `false` | no |
| <a name="input_s3_bucket_prefix"></a> [s3\_bucket\_prefix](#input\_s3\_bucket\_prefix) | Prefix of the bucket that will contain terraform state files. | `string` | `"vendor-tfstate"` | no |
| <a name="input_subdirectories_by_teams"></a> [subdirectories\_by\_teams](#input\_subdirectories\_by\_teams) | Object containing a team name as key and a sub-directory as value. For each team, a specific policy will be created to allow access only to the given sub-directory. | `map(string)` | `{}` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Additional tags to add to all AWS resources. Will be merged with the default tags provided by this module. | `map(string)` | `{}` | no |
| <a name="input_trusted_aws_accounts"></a> [trusted\_aws\_accounts](#input\_trusted\_aws\_accounts) | List of additional AWS account which will be trusted to assume the s3 role. | `list(string)` | `[]` | no |
| <a name="input_trusted_aws_iam_identifiers"></a> [trusted\_aws\_iam\_identifiers](#input\_trusted\_aws\_iam\_identifiers) | List of additional AWS IAM identifiers which will be trusted to assume the s3 role. (ex: A specific role in an other account) | `list(string)` | `[]` | no |
| <a name="input_vendor_prefix"></a> [vendor\_prefix](#input\_vendor\_prefix) | Initials of the company which the project is bootstrapped. | `string` | `"vendor"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dynamodb_arn"></a> [dynamodb\_arn](#output\_dynamodb\_arn) | The ARN of the dynamoDB for terraform state locks. |
| <a name="output_dynamodb_id"></a> [dynamodb\_id](#output\_dynamodb\_id) | The ID of the dynamoDB for terraform state locks. |
| <a name="output_dynamodb_name"></a> [dynamodb\_name](#output\_dynamodb\_name) | The name of the dynamoDB for terraform state locks. |
| <a name="output_iam_policy_arns"></a> [iam\_policy\_arns](#output\_iam\_policy\_arns) | n/a |
| <a name="output_iam_policy_ids"></a> [iam\_policy\_ids](#output\_iam\_policy\_ids) | n/a |
| <a name="output_iam_role_arns"></a> [iam\_role\_arns](#output\_iam\_role\_arns) | n/a |
| <a name="output_iam_role_ids"></a> [iam\_role\_ids](#output\_iam\_role\_ids) | n/a |
| <a name="output_iam_role_unique_ids"></a> [iam\_role\_unique\_ids](#output\_iam\_role\_unique\_ids) | n/a |
| <a name="output_kms_arn"></a> [kms\_arn](#output\_kms\_arn) | The key ARN for the S3 bucket for terraform state files. |
| <a name="output_kms_id"></a> [kms\_id](#output\_kms\_id) | The key id for the S3 bucket for terraform state files. |
| <a name="output_s3_arn"></a> [s3\_arn](#output\_s3\_arn) | The ARN of the S3 bucket for terraform state files. |
| <a name="output_s3_bucket_domain_name"></a> [s3\_bucket\_domain\_name](#output\_s3\_bucket\_domain\_name) | The domain name of S3 bucket for terraform state files. |
| <a name="output_s3_id"></a> [s3\_id](#output\_s3\_id) | The ID of the S3 bucket for terraform state files. |
| <a name="output_s3_region"></a> [s3\_region](#output\_s3\_region) | The region of S3 bucket for terraform state files. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
